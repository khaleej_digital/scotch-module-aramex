<?php
/**
 * @category  Beside
 * @package   Beside_Aramex
 * @author    Emrah Uyanik <emrah.uyanik@redboxdigital.com>
 * @copyright Copyright © 2021 Redbox Digital (http://www.redboxdigital.com)
 */

namespace Beside\Aramex\Block\Checkout;

/**
 * Class CityDataProcessor
 * @package Beside\Aramex\Block\Checkout
 */
class CityDataProcessor implements \Magento\Checkout\Block\Checkout\LayoutProcessorInterface
{
    /**
     * @var \Beside\Aramex\Helper\Data
     */
    private $aramexHelper;

    /**
     * DirectoryDataProcessor constructor.
     * @param \Beside\Aramex\Helper\Data $aramexHelper
     */
    public function __construct(
        \Beside\Aramex\Helper\Data $aramexHelper
    ) {
        $this->aramexHelper = $aramexHelper;
    }

    /**
     * Process js Layout of block
     *
     * @param array $jsLayout
     * @return array
     */
    public function process($jsLayout)
    {
        if (!isset($jsLayout['components']['checkoutProvider']['dictionaries']['city'])) {
            $jsLayout['components']['checkoutProvider']['dictionaries']['city'] = $this->getCityOptions();
        }
        
        /* if (!isset($jsLayout['components']['checkoutProvider']['dictionaries']['cityAr'])) {
            $jsLayout['components']['checkoutProvider']['dictionaries']['cityAr'] = $this->getCityArOptions();
        } */

        return $jsLayout;
    }

    /**
     * Get options list.
     *
     * @return array
     */
    private function getCityOptions()
    {
        $options = $this->aramexHelper->getCities();

        if (count($options) > 0) {
            array_unshift(
                $options,
                ['title' => '', 'value' => '', 'label' => __('Please select a city.')]
            );
        }

        return $options;
    }
    
    /**
     * Get options list.
     *
     * @return array
     */
    private function getCityArOptions()
    {
        $options = $this->aramexHelper->getCitiesAr();

        if (count($options) > 0) {
            array_unshift(
                $options,
                ['title' => '', 'value' => '', 'label' => __('Please select a city.')]
            );
        }

        return $options;
    }
}
