/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * @api
 */
define([
    'underscore',
    'uiRegistry',
    'Magento_Ui/js/form/element/select',
    'Magento_Checkout/js/model/default-post-code-resolver',
    'jquery',
    'mage/translate',
    'mage/utils/wrapper',
    'mage/template',
    'mage/validation',
    'underscore',
    'Magento_Ui/js/form/element/abstract',
    'jquery/ui'
], function (_, registry, Select, defaultPostCodeResolver, $, $t) {
    'use strict';

    return Select.extend({
        defaults: {
            skipValidation: false,
            imports: {
                update: '${ $.parentName }.region_id:value'
            }
        },

        /**
         * @param {String} value
         */
        update: function (value) {

console.log("CITY Update");
console.log(this.parentName + '.' + 'city');

                var city = registry.get(this.parentName + '.' + 'city'),
                            options = city.initialOptions,
                            cityOptions = [];
 

console.log(options);

            $.each(options, function (index, cityOptionValue) {
                if (value == cityOptionValue.region_id) {

                    //console.log(cityOptionValue.name);
                    var name = $.mage.__(cityOptionValue.name);
                    var name2 = $.mage.__(cityOptionValue.name2);
                    /* console.log(name);
                    console.log($t(name)); */

                    var jsonObject = {
                        value: name,
                        title: $t(name),
                        region_id: "",
                        label: name2
                    };
                    cityOptions.push(jsonObject);
                }
            });

//console.log(cityOptions);

            this.setOptions(cityOptions);
        }
    });
});
