<?php
/**
 * @category  Beside
 * @package   Beside_Aramex
 * @copyright Copyright © 2021 Redbox Digital (http://www.redboxdigital.com)
 */

namespace Beside\Aramex\Model;

use Magento\Framework\Model\AbstractExtensibleModel;
use Beside\Aramex\Api\Data\CityInterface;

/**
 * Class City
 * @package Beside\Aramex\Model
 */
class City extends AbstractExtensibleModel implements CityInterface
{
    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Beside\Aramex\Model\ResourceModel\City');
    }

    /**
     * {@inheritdoc}
     */
    public function getCityId()
    {
        return $this->_getData(CityInterface::CITY_ID);
    }

    /**
     * {@inheritdoc}
     */
    public function setCityId($cityId)
    {
        return $this->setData(CityInterface::CITY_ID, $cityId);
    }

    /**
     * {@inheritdoc}
     */
    public function getRegionId()
    {
        return $this->_getData(CityInterface::REGION_ID);
    }

    /**
     * {@inheritdoc}
     */
    public function setRegionId($regionId)
    {
        return $this->setData(CityInterface::REGION_ID, $regionId);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->_getData(CityInterface::NAME);
    }

    /**
     * {@inheritdoc}
     */
    public function setName($name)
    {
        return $this->setData(CityInterface::NAME, $name);
    }
}
