<?php
/**
 * @category  Beside
 * @package   Beside_Aramex
 * @copyright Copyright © 2021 Redbox Digital (http://www.redboxdigital.com)
 */

namespace Beside\Aramex\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Class City
 * @package Beside\Aramex\Model\ResourceModel
 */
class City extends AbstractDb
{
    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init('aramex_city', 'city_id');
    }
}
