<?php
/**
 * @category  Beside
 * @package   Beside_Aramex
 * @copyright Copyright © 2021 Redbox Digital (http://www.redboxdigital.com)
 */

namespace Beside\Aramex\Model\ResourceModel\City;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class Collection
 * @package Beside\Aramex\Model\ResourceModel\City
 */
class Collection extends AbstractCollection
{
    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Beside\Aramex\Model\City', 'Beside\Aramex\Model\ResourceModel\City');
    }
}
