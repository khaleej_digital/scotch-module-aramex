<?php
/**
 * @category  Beside
 * @package   Beside_Aramex
 * @copyright Copyright © 2021 Redbox Digital (http://www.redboxdigital.com)
 */

namespace Beside\Aramex\Setup\Patch\Data;

use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\Patch\PatchRevertableInterface;
use Magento\Framework\App\ResourceConnection;

/**
 * Class AddAramexCityAndRegion
 * @package Beside\Aramex\Setup\Patch\Data
 */
class AddAramexCityAndRegion implements DataPatchInterface, PatchRevertableInterface
{
    /**
     * @var array
     */
    private $cityAndRegions = [
        'AE' => [
            'Abu Dhabi' => [
                'Al Ardiyah',
                'Al Dharbaniyah',
                'Al Dhayd',
                'Al Duss',
                'Al Mafraq',
                'Al Marfa',
                'Al Ruways',
                'Habshan',
                'Mafraq',
                'Sila',
                'Suwayhan',
                'Abu Dhabi',
                'Mussafah',
                'Buteen',
                'Al Maqta',
                'Um Al Nar',
                'Khannur'
            ],
            'Ajman' => [
                'Ajman City',
                'Al Zahra',
                'Bustan',
                'Jarf',
                'Masfut',
                'Mushrif',
                'Rashidya',
                'Sweihan'
            ],
            'Al Ain' => [
                'Al Ain City',
                'Al Hayr',
                'Rima'
            ],
            'Dubai' => [
                'Abu Hayl',
                'Al Qouz',
                'Bur Dubai',
                'Deira',
                'Dubai',
                'Shandaghah',
                'Dubai Internet City',
                'Jumeirah',
                'Karama',
                'Mina Sufooh',
                'Satwa',
                'Reem Community',
                'Jebal Ali',
                'Hatta',
                'Ganthoot',
                'Al Ruwaiya'
            ],
            'Fujairah' => [
                'Qidfa',
                'Kidfa',
                'Al Hay',
                'Furfar',
                'Diba',
                'Diba Al Bay\'a',
                'Diba Al Hesn',
                'Ghurfah',
                'Hassan Dibba',
                'Kalba',
                'Khawr Fakkan',
                'Khawr Kalba',
                'Masafi',
                'Rughaylat',
                'Saqamqam',
                'Siji',
                'Tarif Kalba',
                'Abadilah',
                'Akamiyah',
                'Fujairah'
            ],
            'Ras Al Khaimah' => [
                'Al Jazirah',
                'Khawr Khuwayr',
                'Al Sha\'M',
                'Awanat',
                'Al Manamah',
                'Adhan',
                'Al Fulayyah',
                'Al Hamraniyah',
                'Khatt',
                'Sham',
                'Al Rams',
                'Al Jazirah Al Hamra',
                'Kharran',
                'Nakheel',
                'Ras Al Khaimah city'
            ],
            'Sharjah' => [
                'Al Khashfah',
                'Hamariya',
                'Saabah',
                'Ramlah',
                'Wasit',
                'Al Manak',
                'Al Qazimiya',
                'Al Rafa\'ah',
                'Al Tawoon',
                'Al Wasit',
                'Badiyah',
                'Buteena',
                'Dasman',
                'Dhaid',
                'Ghafia',
                'Ghubaiba',
                'Abu Shagara',
                'Al Azrah',
                'Al Fahiya',
                'Al Falaj',
                'Al Hamriyah',
                'Al Khan',
                'Saif',
                'Samnan',
                'Shahba',
                'Jazzat',
                'Kharayan',
                'Layyah',
                'Maisaloon',
                'Majjarah',
                'Mansoora',
                'Muwafja',
                'Nabbah',
                'Nakheelat',
                'Nasariya',
                'Qasimiya',
                'Ramaqiya',
                'Yarmook',
                'Ramtha',
                'Rolla',
                'Sherqan',
                'Umm Kanoor',
                'Wadi Shi',
                'Sharjah'
            ],
            'Um Al Quiwan' => [
                'Um Al Quiwan',
                'Falaj Al Mulla'
            ]
        ],
        'SA' => [
            'Saudi Arabia' => [
                'Ras Al Kheir',
                'Sarar',
                'Abqaiq',
                'Ain Dar',
                'Anak',
                'Dhahran',
                'Haweyah',
                'Hofuf',
                'Harad',
                'Al Hassa',
                'Jafar',
                'Jubail',
                'Al-Jsh',
                'Khodaria',
                'Khafji',
                'Mubaraz',
                'Nabiya',
                'Ojam',
                'Othmanyah',
                'Qarah',
                'Qatif',
                'Ras Tanura',
                'Rahima',
                'Safwa',
                'Seihat',
                'Safanyah',
                'Tanjeeb',
                'Tarut',
                'Thuqba',
                'Udhaliyah',
                'Uyun',
                'Batha',
                'Mulaija',
                'Salwa',
                'Qariya Al Olaya',
                'Noweirieh',
                'Baqiq',
                'Khobar',
                'Dammam',
                'Al Bada',
                'Al Mada',
                'Subheka',
                'Turaib',
                'Sharourah',
                'Makkah',
                'Wadi Fatmah',
                'Mastura',
                'Jumum',
                'Bahara',
                'Alhada',
                'Rabigh',
                'Shumeisi',
                'Taif',
                'Towal',
                'Jeddah',
                'Amaq',
                'Ash Shuqaiq',
                'Birk',
                'Qahmah',
                'Ahad Masarha',
                'Bareq',
                'Rejal Alma\'A',
                'Samtah',
                'Abu Areish',
                'Baha',
                'Abha',
                'Ahad Rufaidah',
                'Bisha',
                'Biljurashi',
                'Bish',
                'Dhahran Al Janoob',
                'Damad',
                'Darb',
                'Gizan',
                'Harajah',
                'Khamis Mushait',
                'Karboos',
                'Mandak',
                'Mikhwa',
                'Madinah',
                'Mohayel Aseer',
                'Namas',
                'Qunfudah',
                'Sarat Obeida',
                'Tabuk',
                'Wadeien',
                'Yanbu',
                'Najran',
                'Farasan',
                'Hinakeya',
                'Khurma',
                'Rania',
                'Turba',
                'Sabt El Alaya',
                'Balahmar',
                'Balasmar',
                'Wajeh (Al Wajh)',
                'Halat Ammar',
                'Haqil',
                'Tanuma',
                'Umluj',
                'Tatleeth',
                'Tayma',
                'Khaibar',
                'Laith',
                'Muthaleif',
                'Oula',
                'Sabya',
                'Khasawyah',
                'Thabya',
                'Bader',
                'Kara\'A',
                'Aqiq',
                'Gilwa',
                'Hawea/Taif',
                'Mahad Al Dahab',
                'Nimra',
                'Tanda',
                'Wadi Bin Hasbal',
                'Al Mahd',
                'Majarda',
                'Shraie\'E',
                'Ja\'Araneh',
                'Nwariah',
                'Asfan',
                'Shoaiba',
                'Bahrat Al Moujoud',
                'Duba',
                'Atawleh',
                'Khulais',
                'Al Idabi',
                'Badr Al Janoub',
                'Dhahban',
                'Taiba',
                'Al Ardah',
                'Al Ais',
                'Yanbu Nakhil',
                'Zahban',
                'Yanbu Al Baher',
                'Dhurma',
                'Dere\'Iyeh',
                'Al Dalemya',
                'Quwei\'Ieh',
                'Uqlat Al Suqur',
                'Um Aljamajim',
                'Afif',
                'Ayn Fuhayd',
                'Qaysoomah',
                'Tabrjal',
                'Majma',
                'Zulfi',
                'Turaif',
                'Kharj',
                'Riyadh',
                'Badaya',
                'Bukeiriah',
                'Dawadmi',
                'Hafer Al Batin',
                'Midinhab',
                'Muzneb',
                'Onaiza',
                'Sajir',
                'Hail',
                'Arar',
                'Jouf',
                'Buraidah',
                'Domat Al Jandal',
                'Hadeethah',
                'Sakaka',
                'Qurayat',
                'Rafha',
                'Qassim',
                'Khamaseen',
                'Wadi El Dwaser',
                'Kara',
                'Riyadh Al Khabra',
                'Shefa\'A',
                'Sulaiyl',
                'Aflaj',
                'Deraab',
                'Muzahmiah',
                'Shaqra',
                'Alrass',
                'Ghazalah',
                'Baqaa',
                'Shinanh',
                'Alnabhanya',
                'Huraymala',
                'Rwaydah',
                'Hareeq',
                'Horaimal',
                'Oyaynah',
                'Salbookh',
                'Thadek',
                'Sahna',
                'Daelim',
                'Hawtat Bani Tamim',
                'Artawiah',
                'Jalajel',
                'Alghat',
                'Hotat Sudair',
                'Mrat',
                'Qasab',
                'Rowdat Sodair',
                'Thumair',
                'Remah',
                'Tebrak',
                'Al Asyah',
                'Oyoon Al Jawa',
                'Dariyah',
                'Moqaq',
                'Qufar',
                'Balqarn',
                'Al Adari',
                'Abu Ajram',
                'Ar Radifah',
                'As Sulaimaniyah',
                'At Tuwayr',
                'Al Laqayit',
                'An Nabk Abu Qasr',
                'Zallum',
                'Hazm Al Jalamid',
                'An Nafiah',
                'Al Arja',
                'Ash Shaara',
                'Al Qarin',
                'Nisab',
                'Rawdat Habbas',
                'Al Ajfar',
                'An Nuqrah',
                'Ash Shananah',
                'Ash Shamli',
                'Baqa Ash Sharqiyah',
                'Mawqaq',
                'Munifat Al Qaid',
                'Sadyan',
                'Simira',
                'Al Wasayta',
                'Tanumah',
                'Tharmada',
                'Ushayqir',
                'Al Batra',
                'Al Ammarah',
                'Al Midrij',
                'Dulay Rashid',
                'Kahlah',
                'Kubadah',
                'Al Khishaybi',
                'Qusayba',
                'Saqf',
                'Layla',
                'Ar Rishawiyah',
                'Al Fuwaileq',
                'Al Haith',
                'Al Moya',
                'Rvaya Aljamsh',
                'Aba Alworood',
                'Ad Dahinah',
                'Ad Dubaiyah',
                'Al Bijadyah',
                'Al hait',
                'Al Hayathem',
                'Al Hufayyirah',
                'Al Hulayfah As Sufla',
                'Al Khitah',
                'Ar Rafi\'ah',
                'As Sulubiayh',
                'Ash Shimasiyah',
                'Ath Thybiyah',
                'Duhknah',
                'Ghtai',
                'Hedeb',
                'Khairan',
                'King Khalid Military City',
                'Mubayid',
                'Mulayh',
                'Qbah',
                'Shari',
                'Suwayr',
                'Thebea',
                'Siir',
                'Hali',
                'Hubuna',
                'Adham',
                'Abha Manhal',
                'Al Bashayer',
                'Hajrah',
                'Qouz',
                'Al Uwayqilah',
                'Satorp',
                'Addayer',
                'Ras Baridi',
                'Sadal Malik',
                'Samakh',
                'Wadi Faraah',
                'Yuthma',
                'Tablah'
            ]
        ]
    ];

    /**
     * @var ResourceConnection
     */
    private $resourceConnection;

    /**
     * AddAramexCityAndRegion constructor.
     * @param ResourceConnection $resourceConnection
     */
    public function __construct(
        ResourceConnection $resourceConnection
    ) {
        $this->resourceConnection = $resourceConnection;
    }

    /**
     * Create Tax Rates and Rules
     */
    public function apply()
    {
        /** @var \Magento\Framework\DB\Adapter\AdapterInterface $connection */
        $connection = $this->resourceConnection->getConnection();
        $cityTableName = $this->resourceConnection->getTableName('aramex_city');
        $regionTableName = $this->resourceConnection->getTableName('directory_country_region');

        foreach ($this->cityAndRegions as $countryCode => $regions) {
            $counter = 0;
            foreach ($regions as $region => $cities) {
                $counter++;
                $data = ['country_id' => $countryCode, 'code'=> $countryCode . '-' . $counter ,'default_name' => $region];
                $connection->insert(
                    $regionTableName,
                    $data
                );

                /** @var \stdClass $regionData */
                $regionData = $connection->select()
                    ->from($regionTableName)
                    ->where('default_name = ?', $region)
                    ->query()
                    ->fetchObject();
                $regionId = $regionData->region_id;

                $cityData = [];
                foreach ($cities as $city) {
                    $cityData[] = ['region_id' => $regionId, 'name' => $city];
                }

                $connection->insertMultiple(
                    $cityTableName,
                    $cityData
                );
            }
        }
    }

    /**
     * @inheritDoc
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function revert()
    {
        return [];
    }
}
