<?php
/**
 * @category  Beside
 * @package   Beside_Aramex
 * @author    Emrah Uyanik <emrah.uyanik@redboxdigital.com>
 * @copyright Copyright © 2021 Redbox Digital (http://www.redboxdigital.com)
 */

namespace Beside\Aramex\Helper;

use Beside\Aramex\Model\ResourceModel\City\CollectionFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Cache\Type\Config;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\Locale\Resolver;

class Data extends AbstractHelper
{
    const XML_PATH_ARAMEX_TRACKING_LINK = 'carriers/aramex/base_tracking_link';
    /**
     * @var array
     */
    private $citiesArray = [];

    private $citiesArrayAr = [];

    private $localeResolver;

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;
    /**
     * @var Config
     */
    private $configCacheType;
    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * Data constructor.
     * @param CollectionFactory $collectionFactory
     * @param Config $configCacheType
     * @param SerializerInterface $serializer
     * @param Context $context
     */
    public function __construct(
        \Beside\Aramex\Model\ResourceModel\City\CollectionFactory $collectionFactory,
        \Magento\Framework\App\Cache\Type\Config $configCacheType,
        \Magento\Framework\Serialize\SerializerInterface $serializer,
        \Magento\Framework\App\Helper\Context $context,
        StoreManagerInterface $storeManager,
        Resolver $localeResolver
    ){
        $this->collectionFactory = $collectionFactory;
        $this->configCacheType = $configCacheType;
        $this->serializer = $serializer;
        $this->storeManager = $storeManager;
        $this->localeResolver = $localeResolver;
        parent::__construct($context);
    }

    /**
     * @return array
     */
    public function getCities()
    {

        $currentLocaleCode = $this->localeResolver->getLocale();
        $languageCode = strstr($currentLocaleCode, '_', true);
        $this->_logger->info("LANGAGE CODE : " . $languageCode);


        if (!$this->citiesArray) {
            $cacheKey = 'DIRECTORY_CITIES_JSON';

            $cacheTag = $this->configCacheType->getTag();

            $this->_logger->info("CACHE TAGGGG : " . $cacheTag);
            

            //$this->configCacheType->clean("matchingTag", array($cacheKey, $cacheTag));

            $this->_logger->info("matchingTag : CLEANNN");


            //$json = $this->configCacheType->load($cacheKey);
            $json = "";

            $this->_logger->info($json);


            if (empty($json)) {
                $cityCollection = $this->collectionFactory->create();
                foreach ($cityCollection as $city) {
                    $this->citiesArray[] = [
                        'value' => $city->getId(),
                        'name' => __($city->getName()),
                        'region_id' => $city->getRegionId()
                    ];
                }
                $json = $this->serializer->serialize($this->citiesArray);
                if ($json === false) {
                    $json = 'false';
                }

                $this->configCacheType->save($json, $cacheKey);
            } else {
                $this->citiesArray = $this->serializer->unserialize($json);
            }
        }


        $this->_logger->info(print_r($this->citiesArray, true));


        return $this->citiesArray;
    }


    public function getCitiesAr()
    {

        $currentLocaleCode = $this->localeResolver->getLocale();
        $languageCode = strstr($currentLocaleCode, '_', true);

        //if (!$this->citiesArrayAr && $languageCode == "ar") {
        if ($languageCode == "ar") {
            $cacheKey = 'DIRECTORY_CITIES_JSON_AR';
            $json = $this->configCacheType->load($cacheKey);
            if (empty($json)) {
                $cityCollection = $this->collectionFactory->create();
                foreach ($cityCollection as $city) {
                    $this->citiesArrayAr[] = [
                        'value' => $city->getId(),
                        'name' => __($city->getName()),
                        'region_id' => $city->getRegionId()
                    ];
                }
                $json = $this->serializer->serialize($this->citiesArrayAr);
                if ($json === false) {
                    $json = 'false';
                }

                $this->configCacheType->save($json, $cacheKey);
            } else {
                $this->citiesArrayAr = $this->serializer->unserialize($json);
            }
        }

        return $this->citiesArrayAr;
    }


    /**
     * @return string
     */
    public function getTrackingUrl()
    {
        return (string) $this->scopeConfig->getValue(
            self::XML_PATH_ARAMEX_TRACKING_LINK,
            ScopeInterface::SCOPE_STORES,
            $this->storeManager->getStore()->getCode()
        );
    }
}
