<?php
/**
 * @category  Beside
 * @package   Beside_Aramex
 * @author    Emrah Uyanik <emrah.uyanik@redboxdigital.com>
 * @copyright Copyright © 2021 Redbox Digital (http://www.redboxdigital.com)
 */

namespace Beside\Aramex\Plugin\Block\Checkout;

/**
 * Class LayoutProcessor
 * @package Beside\Aramex\Plugin\Block\Checkout
 */
class LayoutProcessor
{
    /**
     * @var \Beside\Aramex\Helper\Data
     */
    private $aramexHelper;

    /**
     * DirectoryDataProcessor constructor.
     * @param \Beside\Aramex\Helper\Data $aramexHelper
     */
    public function __construct(
        \Beside\Aramex\Helper\Data $aramexHelper
    ) {
        $this->aramexHelper = $aramexHelper;
    }

    /**
     * @param LayoutProcessor $subject
     * @param array $result
     * @return array
     */
    public function afterProcess(
        \Magento\Checkout\Block\Checkout\LayoutProcessor $subject,
        array $result
    ) {
        $result['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']['children']['city'] = $this->getConfig();
        $result['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']['children']['telephone']['validation']['min_text_length'] = '12';
        $result['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']['children']['telephone']['validation']['max_text_length'] = '15';

        return $result;
    }

    /**
     * @return $field
     */
    private function getConfig()
    {
        $field = [
            'component' => 'Beside_Aramex/js/city',
            'config' => [
                'customScope' => 'shippingAddress',
                'template' => 'ui/form/field',
                'elementTmpl' => 'ui/form/element/select',
                'id' => 'city'
            ],
            'label' => 'City',
            'value' => '',
            'dataScope' => 'shippingAddress.city',
            'provider' => 'checkoutProvider',
            'sortOrder' => 80,
            'customEntry' => null,
            'visible' => true,
            'options' => [ ],
            'filterBy' => [
                'target' => '${ $.provider }:${ $.parentScope }.region_id',
                'field' => 'region_id'
            ],
            'validation' => [
                'required-entry' => true
            ],
            'id' => 'city',
            'imports' => [
                'initialOptions' => 'index = checkoutProvider:dictionaries.city',
                'setOptions' => 'index = checkoutProvider:dictionaries.city'
            ]
        ];


        return $field;
    }
}
